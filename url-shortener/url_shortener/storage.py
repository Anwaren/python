"""
В этом модуле будут функции для работы с БД

Запрос на удаление
DELETE FROM table_name WHERE colunm_name = value
"""
import sqlite3
import os.path as Path

SQL_INSERT_URL = '''
    INSERT INTO shortener (original_url)
    VALUES (?)
'''

SQL_UPDATE_SHORT_URL = '''
    UPDATE shortener SET short_url =? WHERE id =?
'''


SQL_SELECT_ALL = '''
    SELECT
        id, original_url, short_url, created
    FROM 
        shortener
'''

SQL_SELECT_URL_BY_PK = SQL_SELECT_ALL + ' WHERE id = ?'

SQL_SELECT_URL_BY_ORIGIN = SQL_SELECT_ALL + ' WHERE original_url = ?'


def dict_factory(cursor, row):
    '''print(cursor.description)
    print(row)'''
    d = dict()
    for index, column in enumerate(cursor.description):
        d[column[0]] = row[index]
    return d

def connect(db_name=':memory:'):
    """Устанавливает соединение с БД и возвращает объект соединения"""
    conn = sqlite3.connect(db_name)
    #Для нужного нам вида выборки из бд
    conn.row_factory = dict_factory
    return conn


def initialize(conn):
    """Инициализация структуры БД"""
    script_path = Path.join(Path.dirname(__file__), 'shortener.sql')

    with conn, open(script_path) as f:
        conn.executescript(f.read())

def add_url(conn, url):
    """Сохраняет url в БД"""
    url = url.rstrip('/')

    if not url:
        """Выбрасываем исключение"""
        raise RuntimeError("Invalid URL")

    found = find_url_by_original(conn, url)

    if found:
        #return found.get('short_url')
        return found[2]


    with conn:  # В блоке контекста - транзакиция!
        cursor = conn.execute(SQL_INSERT_URL, (url, ))
        pk = cursor.lastrowid
        short_url = '/{}'.format(pk)

        conn.execute(SQL_UPDATE_SHORT_URL, (short_url, pk))

        return short_url


def find_url_by_original(conn, url):
    """Выбирает из БД URL-адрес по оригинальному URL"""
    with conn:
        cursor = conn.execute(SQL_SELECT_URL_BY_ORIGIN, (url, ))

    return cursor.fetchone()


def find_all(conn):
    with conn:
        cursor = conn.execute(SQL_SELECT_ALL)

    return cursor.fetchall()