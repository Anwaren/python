"""Обработчики действий и функция main"""

import sys
from . import storage
from datetime import datetime


get_connection = lambda: storage.connect('shortener.sql')


def action_add():
    """Добавить url адрес"""
    url = input('\nВведите URL-адрес: ')
    #Не забыть проверить входные данных!
    if url:
        with get_connection() as conn:
            short_url = storage.add_url(conn, url)
            print('Короткий URL: {}'.format(short_url))
    else:
        print("URL-адрес не может быть пустым")
    


def action_find():
    """Найти оригинальный url адрес"""


def action_find_all():
    """Вывести все url адреса"""
    with get_connection() as conn:
        template = '{url[id]} - {url[short_url]} - {url[created]:%d/%m/%Y}'
        for url in storage.find_all(conn):
            url['created'] = datetime.strptime(url['created'], '%Y-%m-%d %H:%M:%S')
            print(template.format(url = url))


def menu():
    """Показать меню"""


def action_menu():
    """Выйти"""
    print('''
Введите команду

1. Добавить URL-адрес
2. Найти оригинальный URL-адрес
3. Вывести все URL-адреса
m. Показать меню
q. Выйти 
    ''')


def action_exit():
    """Выйти"""
    sys.exit(0)

def main():
    """Главная функция приложения"""
    with get_connection() as conn:
        storage.initialize(conn)

    actions = {
        '1':action_add,
        '2':action_find,
        '3':action_find_all,
        'm':action_menu,
        'q':action_exit
    }

    action_menu()

    while 1:
        cmd = input('\nВведите команду: ')
        action = actions.get(cmd)

        if action:
            action()
        else:
            print('Не известная команда')