"""Ветвление

a = 1
b = 2

if a > b:
    print("a > b")
elif a == b:
    print("a = b")
else:
    print("a < b")
"""

"""Тернарный оператор

a = input()

if a.isdigit():
    a = int(a)
else:
    a = 0

a = int(a) if a.isdigit() else 0

print(a)

"""

"""Циклы

i = 1
while i:
    if i == 10:
        break

    i += 1

    if i % 2:
        continue

    print(i)
"""
ls = [1,2,3]

for i, v in enumerate(ls): # enumerate возвращает итератор кортеж
    print(i, v)

d = {
    "key" : "value",
    "key2" : "value2",
}

for i, v in d.items(): # d.items() возвращает итератор кортеж
    print(i,v)

"""Срезы"""

s = "Hello, Python!"
print(s[7:])
print(s[7:-1])
print(s[-8:])

print(s[::-1])  #Реверс строки


s = ''      #Проблема выделения памяти, так лучше не делать

for i in range(10):
    s += str(i)
print(s)

# Делаем так

l = []

for i in range(10):
    l.append(str(i))

print(''.join(l))

lst = range(1, 20)
lst = list(lst)

print(lst)

# Копироание списка через срез
lst2 = lst[:]
print(lst2)


    

