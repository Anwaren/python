"""
Импорт модуля целиком:
import math

Частичный импорт
from math import pi

Псевдоним (aliase)
from math import pi as PI

Импорт со звездочкой (лучше не использовать)
from math import *

Просмотреть что находиться в модуле можно dir(math)
math.__dict__

"""
import math
from math import pi

a = 555
lst = [1,2,3]


def debug():
    print(a, lst)

#Функция для вычисления площади квадрата
def calc_square_area(a):
    return a**2


#Площадь прямоугольника
def calc_rectangle_area(a,b):
    return a*b


#Площадь треугольника
def calc_triangle_area(a,b,c):
    p = (a + b + c)/2
    return (p*(p-a)*(p-b)*(p-c))**0.5


#Площадь круга
def calc_circle_area(r):
    return r**2 * pi


# Для того, что бы при импорте через * не импортировать все, пишем секцию __all__
__all__ = [    
    'calc_square_area',
    'calc_rectangle_area',
    'calc_triangle_area',
    'calc_circle_area'
]

print(__name__)
if __name__ == '__main__':  #Если модуль используется не как модуль а как исполняемый файл
    print("Запускаем тесты")