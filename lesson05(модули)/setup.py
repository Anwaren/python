# В файле setup описываем всю информацию для пакетного менеджера
from setuptools import setup

"""
Нужно вызвать функцию setup с именнованными агрументами
name        - Название (имя) пакета
version     - Версия пакета (1.0.0)
-Необязательно
description - Краткое описание пакета
url         - URL-адрес пакета
license     - Лицензия (требуется верная запись)
author      - Имя автора (лучше как в git)
author_email- Email автора
py_modules  - Модули, которые нужно скопировать при установке
packages    - Пакеты, которые нужно скопировать при установке 
              (подпакеты нужно указыват явно)
scripts     - Запускаемые из командной строки скрипты
install_requires - Список прямых зависимостей от других пакетов
"""

setup(
    name = "mega-math",
    version = "1.0.0b1",
    description = "Collection bla-bla-bla",
    url = "https:\\githab.com\\Anwaren\\mega-math",
    license = "Apache 2.0",
    author = "Anwaren",
    author_email = "mail@mail.mail",
    #py_modules = ['square'],
    packages = ['mega_math'],
    #install_requires = ['Flask']
)

#python setup.py sdist