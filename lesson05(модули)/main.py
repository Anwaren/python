"""
Модули

PYTHONPATH -

import sys 
sys.path - позволяет посмотреть пути откуда можем делать импорт

Желательно в главном модуле меньше кода, остальное в модулять, т.к
виртуальная  машина питона будет компилировать модули в байт код - увеличиваем скорость, но машина 
не компилирует главный файл
"""

"""
При таком импорте ссылочные типы меняются (lst)
from square import calc_square_area, a, lst, debug

debug()
a += 1
lst.append('test2')
debug()
"""

import square

square.debug()
square.a += 1
square.lst.append('test2')
square.debug()