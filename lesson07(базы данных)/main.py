"""
Базы данных
SQLite3

PRIMARY KEY - первичный ключ

Алгоритм взаимодействия с БД:
1. Установка соединения с сервером БД
    1.1. Опционально (для СУБД), выбрать базу данных
2. Выполнение запроса
    2.1. Получить объект "курсора" (опционально)
    2.2. Выполнить запрос с помощью execute
    2.3. Если запрос на изменение данных или структуры БД:
        2.3.1. Нужно зафиксировать изменения (commit)
    2.4. Если запрос на получение данных (SELECT)
        2.4.1. Фактические данные нужно получить (fetchall() - все) 

"""
import sqlite3

SQL_CREATE_TABLE_USER = '''
    CREATE TABLE user (
        id INTEGER PRIMARY KEY AUTOINCREMENT,
        name VARCHAR(50) NOT NULL DEFAULT "",
        email TEXT NOT NULL,
        password TEXT NOT NULL,
        active BOOLEAN NOT NULL DEFAULT 0
    )
'''

SQL_INSERT_USER = """
    INSERT INTO user (
        name, email, password
    ) VALUES(
        ?, ?, ?
    )
"""

SQL_SELECT_ALL_USERS = '''
    SELECT 
        id, name, email, active
    FROM user
'''

with sqlite3.connect(':memory:') as conn: # Создаем базу в оперативной памяти
    """ Длинная запись
    cursor = conn.cursor()
    cursor.execute(SQL_CREATE_TABLE_USER)
    cursor.commit() # Без контекстного менеджера не вызовется автоматически!"""

    # Короткая запись для создания таблицы
    conn.execute(SQL_CREATE_TABLE_USER) # Можно cursor = conn.execute(...)

    conn.execute(SQL_INSERT_USER, ('A', 'a@a', 'aaa')) # второй параметр - кортеж (если 1 элемент, то надо , ('A', ))
    
    data = ('B', 'b@b', 'bbb')
    conn.execute(SQL_INSERT_USER, data)

    conn.commit()

    crusor = conn.execute(SQL_SELECT_ALL_USERS)

    #data = crusor.fetchall()    #Забираем все в список из кортежей

    

    for row in crusor:
        i = 0

    crusor -= 1

    for row in crusor:
        i = 0
        
