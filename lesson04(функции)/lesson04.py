"""
Функции def name(arg): 
(name - задаем в snake case my_first_fun)
pass - для задания пустого блока кода
"""

def hello_python():
    print("Hello, python!")

hello_python()

print(type(hello_python)) # <class 'function'>

"""Аргументы функции:
    Нужны для передачи каких либо значений ей"""

def say(name):
    print("Hello,", name, "!")
    print(''.join(["Hello,", str(name), "!"]))
    print('Hello, {}!!!'.format(name))         # Желательно использовать format для строк

say("bitch")

"""Передача по значению и ссылке"""

# Строка передается по значению (как и любой неизменяемый тип) - исходная строка не изменяется
# List передается по ссылке (будет изменятся)

def parse(src, output):
    src = src.strip('.')    # Убирает заданный символ с начала и с конца строки (' ' по умолчанию)

    for i in src.split():    # split - делает из строки list (через ' ' по умолчанию)
        output.append(i)

src = 'Python is a programming language...'
lst = ['hello']

print(src, lst)
parse(src, lst)
print(src, lst)

"""Возвращаемые значения функции"""

def my_pow(x, n):
    return x ** n

print(my_pow(2, 3))


def multi(x, y, n):
    if n > 2:
        return x ** n, y ** n
    else:
        return x ** n

print(multi(2,3,4))
print(multi(3,4,2))

"""Аргументы со значением по умолчанию"""

def increment(x, step=1):
    return x + step

print(increment(12, 5), increment(12), increment(step=2, x=11))

def validate(data, errors=None):
    if errors is None:
        errors = []
    errors.append('error in: {}'.format(data))
    print(errors)

validate('123')
validate('123')

"""Переменное число аргументов"""

def summa(*args):       # Через звездочку можно передать позиционные элементы (через tuple)
    print(type(args))

    r = 0

    for i in args:
        r += i

    return r

print('Сумма чисел:', summa(1,2,3,4,5,6,7,8,9,10))

def f(a, b, c, e=None, d=None):
    return

f(0, 1, 2, e=4, d=5)
f(e=4, d=5, a=0, b=1, c=2)

def demo_f(*args, **kwargs):     # Через ** можно передать именнованные элементы (через dictonary)
    print(type(args), type(kwargs))
    print(args, kwargs)

demo_f(0,2,6, a=3, b=4, c='vv')

"""Анонимная функция"""

lambda x: x ** 0.5

def my_filter(x):
    return not x % 2

lst = list(range(10, 31))

#lst = list(filter(my_filter, lst))
lst = list(filter(lambda i : i % 2, lst))

print(lst)

"""Замыкания"""
def trim(chars= None):  #Каррирование(частичное применение)
    def f(s):
        return s.strip(chars)
    return f                    #Возврат ссылки на функцию

spaces_trim = trim()
slashes_trim = trim('\\/')

print(type(spaces_trim), spaces_trim)
print(type(slashes_trim), slashes_trim)

print(spaces_trim("   Hello.,  world  "))

"""Рекурсивная функция"""

#1. Прямая рекурсия
def factorial(x):
    if x != 0:
        return x*(factorial(x-1))
    else:
        return 1


print(factorial(5))

#2. Косвенная рекурсия 
'''
    def f1():
        f2()

    def f2():
        f1()
'''

"""Области видимости и время жизни переменной"""

g = 666

def wrapper():
    external = 777
    lst = [1,2,3]

    def func(lst):
        global g
        g+= 1
        nonlocal external
        external += 1

        lst.append(4)
    
    func()
    print(g, external, lst)

wrapper()
wrapper()